using System; 
using System.Collections.Generic;
using iMessengerCoreAPI.Models;

namespace iMessengerCoreAPI.Service
{
    public interface ISearchDialogByClient
    {
        Guid Search(List<Guid> listClient);
    }
}