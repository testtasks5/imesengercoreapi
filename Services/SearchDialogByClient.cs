using System;
using System.Linq;
using System.Collections.Generic;
using iMessengerCoreAPI.Models;

namespace iMessengerCoreAPI.Service
{
    /// <summary>
    /// Сервис по поиску общего диалога
    /// </summary>
    public class SearchDialogByClient : ISearchDialogByClient
    {
        private readonly MyAppContext _context;

        public SearchDialogByClient(MyAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Поиск клиентов состоящих в одном диалоге
        /// </summary>
        /// <param name="listClient"></param>
        /// <returns>Возвращает Guid общего диалога. В противном случаее вернется пустой Guid</returns>
        public Guid Search(List<Guid> listClient)
        {
            Guid generalGuid = Guid.Empty;

            var listClientFromQuery = _context.RGDialogsClients
                        .Where(x => listClient.Contains(x.IDClient)).ToList();
        
            var findGuid = listClientFromQuery.GroupBy(x =>x.IDRGDialog)
                        .Where(g => g.Count()==listClient.Count)
                        .Select(y => y.Key)
                        .ToList();
            
            if(findGuid != null && findGuid.Count >= 1)
                generalGuid = findGuid.FirstOrDefault();
            return generalGuid;
        }
    }
}