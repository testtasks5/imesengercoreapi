using System;
using iMessengerCoreAPI.Models;
using iMessengerCoreAPI.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace iMessengerCoreAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly ISearchDialogByClient _searchDialogByClient;

        public SearchController(ISearchDialogByClient searchDialogByClient)
        {
            _searchDialogByClient = searchDialogByClient;
        }
        [HttpGet]
        [Route("GetById")]
        public ActionResult<List<Guid>> GetById([FromBody]List<Guid> list)
        {
            var result = _searchDialogByClient.Search(list);
            return Ok(result);
        }
    } 
}