using Microsoft.EntityFrameworkCore;

namespace iMessengerCoreAPI.Models
{
    public class MyAppContext : DbContext
    {
        public MyAppContext(DbContextOptions<MyAppContext> options) : base(options)
        {}

        public DbSet<RGDialogsClient> RGDialogsClients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RGDialogsClient>()
                .HasKey(k => k.Id);
        }
    }
}