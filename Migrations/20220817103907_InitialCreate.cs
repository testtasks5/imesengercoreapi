﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iMessengerCoreAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RGDialogsClients",
                columns: table => new
                {
                    IDUnique = table.Column<Guid>(nullable: false),
                    IDRGDialog = table.Column<Guid>(nullable: false),
                    IDClient = table.Column<Guid>(nullable: false),
                    DateEvent = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RGDialogsClients");
        }
    }
}
